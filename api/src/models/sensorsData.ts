// import mongoose from "mongoose";

// const SensorsDataSchema = new mongoose.Schema(
//   {
//     humidity: { type: Number, required: true },
//     temperature: { type: Number, required: true },
//     luminosity: { type: Number, required: true },
//     motions: { type: String, required: true },
//     intrusions: { type: String, required: true },
//   },
//   { collection: "IoT-Ynov-Database" }
// );

// export const SensorDataModel = mongoose.model("sensor-data", SensorsDataSchema);

// export function getSensorsData() {
//   return SensorDataModel.find();
// }

import { ObjectId } from "mongodb";

export default interface SensorsData {
  _id?: ObjectId;
  humidity: number;
  temperature: number;
  luminosity: number;
  motions: string;
  intrusions: string;
}
