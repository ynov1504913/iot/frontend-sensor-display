/********************* Config with auth and user routes *********************

import express from "express";
import http from "http";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import compression from "compression";
import cors from "cors";
import mongoose from "mongoose";
import router from "./router";
import * as dotenv from "dotenv";

dotenv.config();

const app = express();

app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  cors({
    credentials: true,
  })
);

const server = http.createServer(app);
server.listen(process.env.PORT, () => {
  console.log(`Server is running on port http://localhost:${process.env.PORT}`);
});

mongoose.Promise = Promise;
mongoose.connect(process.env.DB_CONN_STRING);
mongoose.connection.on("error", (err: Error) => console.log(err));

app.use("/", router());

********************* Config with SensorsData routes *********************/

import express from "express";
import http from "http";
import { connectToDatabase } from "./service/database";
import router from "./router";
import * as dotenv from "dotenv";
import compression from "compression";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import cors from "cors";

dotenv.config();

const app = express();
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  cors({
    credentials: true,
  })
);

const server = http.createServer(app);
server.listen(process.env.PORT, () => {
  console.log(`Server is running on port http://localhost:${process.env.PORT}`);
});

try {
  connectToDatabase();
  app.use("/", router());
} catch (error) {
  console.error("Database connection failed", error);
  process.exit();
}
