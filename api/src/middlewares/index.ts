import express from "express";
import { get, merge } from "lodash";

import { getUserBySessionToken } from "../models/users";

export async function isOwner(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const { id } = req.params;
    const currentUserId = get(req, "identity._id") as string;

    if (!currentUserId) {
      return res.status(403).json({ message: "Unauthorized - Can't get your user identity" }).end();
    }

    if (currentUserId.toString() !== id) {
      return res
        .status(403)
        .json({ message: "Unauthorized - You're not the owner of this account" })
        .end();
    }

    return next();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}

export async function isAuthenticated(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const sessionToken = req.cookies["sessionToken"];

    if (!sessionToken) {
      return res.status(401).json({ message: "Unauthorized - You're not connected" }).end();
    }

    const existingUser = await getUserBySessionToken(sessionToken);

    if (!existingUser) {
      return res
        .status(401)
        .json({ message: "Unauthorized - Logged-in user does not exist anymore" })
        .end();
    }

    merge(req, { identity: existingUser });

    return next();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}
