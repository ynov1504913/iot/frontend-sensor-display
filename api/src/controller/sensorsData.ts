import express from "express";
import { ObjectId } from "mongodb";
import { collections } from "../service/database";
import SensorsData from "models/sensorsData";

export async function getAllSensorsData(req: express.Request, res: express.Response) {
  try {
    // Call find with an empty filter object, meaning it returns all documents in the collection. Saves as SensorsData array to take advantage of types
    const sensorsData = await collections.sensorsData.find({}).toArray();
    return res.status(200).json(sensorsData);
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}

export async function postSensorData(req: express.Request, res: express.Response) {
  try {
    const newSensorData = req.body as SensorsData;
    const result = await collections.sensorsData.insertOne(newSensorData);

    if (result) {
      return res
        .status(201)
        .send(`Successfully created a new sensorData with id ${result.insertedId}`);
    } else {
      return res.status(500).send("Failed to create a new game.");
    }
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}

export async function deleteSensorData(req: express.Request, res: express.Response) {
  const id = req?.params?.id;

  try {
    const query = { _id: new ObjectId(id) };
    const result = await collections.sensorsData.deleteOne(query);

    if (result && result.deletedCount) {
      return res.status(202).send(`Successfully removed sensor data with id ${id}`);
    } else if (!result) {
      return res.status(400).send(`Failed to remove sensor data with id ${id}`);
    } else if (!result.deletedCount) {
      return res.status(404).send(`Sensor data with id ${id} does not exist`);
    }
  } catch (error) {
    console.error(error.message);
    return res.status(400).send(error.message);
  }
}

export async function deleteAllSensorsData(req: express.Request, res: express.Response) {
  try {
    const result = await collections.sensorsData.deleteMany({});

    if (result && result.deletedCount) {
      return res.status(202).send(`Successfully removed all sensors data`);
    } else if (!result) {
      return res.status(400).send(`Failed to remove all sensors data`);
    }
  } catch (error) {
    console.error(error.message);
    return res.status(400).send(error.message);
  }
}
