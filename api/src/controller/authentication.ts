import express from "express";
import { getUserByEmail, createUser, getUserById } from "../models/users";
import { random, authentication } from "../helpers";
import { get } from "lodash";

export async function logout(req: express.Request, res: express.Response) {
  try {
    const currentUserId = get(req, "identity._id") as string;

    if (!currentUserId) {
      return res.status(403).json({ message: "Unauthorized - Can't get your user identity" }).end();
    }
    const user = await getUserById(currentUserId.toString());

    if (!user) {
      return res.status(400).json({ message: "User does not exist" }).end();
    }

    user.authentication.sessionToken = null;

    await user.save();

    res.clearCookie("sessionToken", {
      domain: "localhost",
      path: "/",
    });

    return res.status(200).json(user).end();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}

export async function login(req: express.Request, res: express.Response) {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).json({ message: "Missing fields" }).end();
    }

    const user = await getUserByEmail(email).select(
      "+authentication.password +authentication.salt"
    );

    if (!user) {
      return res.status(400).json({ message: "User does not exist" }).end();
    }

    const expectedHash = authentication(user.authentication.salt, password);

    if (expectedHash !== user.authentication.password) {
      return res.status(403).json({ message: "Invalid password" }).end();
    }

    const salt = random();
    user.authentication.sessionToken = authentication(salt, user._id.toString());

    await user.save();

    res.cookie("sessionToken", user.authentication.sessionToken, {
      domain: "localhost",
      path: "/",
    });

    return res.status(200).json(user).end();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}

export async function register(req: express.Request, res: express.Response) {
  try {
    const { email, password, username } = req.body;

    if (!email || !password || !username) {
      return res.status(400).json({ message: "Missing fields" }).end();
    }

    const existingUser = await getUserByEmail(email);

    if (existingUser) {
      return res.status(400).json({ message: "User already exists" }).end();
    }

    const salt = random();
    const user = await createUser({
      email,
      username,
      authentication: {
        salt,
        password: authentication(salt, password),
      },
    });

    return res.status(201).json(user).end();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
}
