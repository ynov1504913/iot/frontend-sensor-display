import express from "express";
import { register, login, logout } from "../controller/authentication";
import { isAuthenticated } from "../middlewares";

export default function (router: express.Router) {
  router.post("/auth/register", register);
  router.post("/auth/login", login);
  router.post("/auth/logout", isAuthenticated, logout);
}
