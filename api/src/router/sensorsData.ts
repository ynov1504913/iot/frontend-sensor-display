import express from "express";

import {
  getAllSensorsData,
  postSensorData,
  deleteAllSensorsData,
  deleteSensorData,
} from "../controller/sensorsData";

export default function (router: express.Router) {
  router.get("/sensors-data", getAllSensorsData);
  router.post("/sensors-data", postSensorData);
  router.delete("/sensors-data/all", deleteAllSensorsData);
  router.delete("/sensors-data/:id", deleteSensorData);
}
