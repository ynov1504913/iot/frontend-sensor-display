import express from "express";
import authentication from "./authentication";
import users from "./users";
import sensorsData from "./sensorsData";

const router = express.Router();

export default function (): express.Router {
  authentication(router);
  users(router);
  sensorsData(router);
  return router;
}
