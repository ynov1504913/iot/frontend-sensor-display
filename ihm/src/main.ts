import { createApp } from "vue";
import App from "./App.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faDoorOpen,
  faDoorClosed,
  faPerson,
  faPersonRunning,
  faTrashCan,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faDoorOpen, faDoorClosed, faPerson, faPersonRunning, faTrashCan);

createApp(App).component("font-awesome-icon", FontAwesomeIcon).mount("#app");
