import type { ChartData, ChartOptions } from "chart.js";

/**
 * Met à jour les data de la LineChart
 * @param label Titre de la donnée à afficher
 * @param labels Axe des abscisses
 * @param backgroundColor Couleur du graphique
 * @param data Axe des ordonnées
 * @returns un objet {@link ChartData<"line">} contenant les data de la LineChart
 */
export function updateData(
  label: string,
  labels: string[],
  backgroundColor: string,
  data: number[]
): ChartData<"line"> {
  return {
    labels: labels,
    datasets: [
      {
        label: label,
        backgroundColor: backgroundColor,
        data: data,
      },
    ],
  };
}

/**
 * Met à jour les options de la LineChart
 * @returns un objet {@link ChartOptions<"line">} contenant les options de la LineChart
 */
export function updateOptions(): ChartOptions<"line"> {
  return {
    responsive: true,
    maintainAspectRatio: false,
  };
}
