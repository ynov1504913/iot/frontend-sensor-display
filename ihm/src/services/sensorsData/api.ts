import axios from "axios";
import type { AxiosResponse } from "axios";
// const API_URL = process.env.API_URL;
const VITE_API_URL = import.meta.env.VITE_API_URL;
const SENSORS_DATA_PATH = "/sensors-data";
const PATH_ALL = "/all";

export async function getSensorsDataFromApi(): Promise<AxiosResponse> {
  return axios.get(VITE_API_URL + SENSORS_DATA_PATH);
}

export async function deleteAllSensorsDataFromApi(): Promise<AxiosResponse> {
  return axios.delete(VITE_API_URL + SENSORS_DATA_PATH + PATH_ALL);
}
