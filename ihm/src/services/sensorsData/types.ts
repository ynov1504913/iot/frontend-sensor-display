import type { ObjectId } from "mongodb";

export interface SensorsData {
  _id: string;
  humidity: number;
  temperature: number;
  luminosity: number;
  motions: string;
  intrusions: string;
}
